<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    $gateway = new Braintree\Gateway([
        'environment' => config('services.braintree.environment'),
        'merchantId' => config('services.braintree.merchantId'),
        'publicKey' => config('services.braintree.publicKey'),
        'privateKey' => config('services.braintree.privateKey')
    ]);

    $token=$gateway->clientToken()->generate();

    return view('welcome',['token'=>$token]);
});


Route::get('/hosted', function () {
    $gateway = new Braintree\Gateway([
        'environment' => config('services.braintree.environment'),
        'merchantId' => config('services.braintree.merchantId'),
        'publicKey' => config('services.braintree.publicKey'),
        'privateKey' => config('services.braintree.privateKey')
    ]);
    $token = $gateway->ClientToken()->generate();
    return view('host-method', [
        'token' => $token
    ]);
});




Route::get('/make-payment',function(\Illuminate\Http\Request $request) {


    //$result->success;


    //$result->customer->id;

    return  config('services.braintree.merchantId');


    $amount = $request->amount;
    $nonce = $request->payment_method_nonce;


    $gateway = new Braintree\Gateway([
        'environment' => config('services.braintree.environment'),
        'merchantId' => config('services.braintree.merchantId'),
        'publicKey' => config('services.braintree.publicKey'),
        'privateKey' => config('services.braintree.privateKey')
    ]);

    //$customer = $gateway->customer()->find('652694121');

    $payment= $gateway->transaction()->sale(
        [
            'customerId' => '652694121',
            'amount' => '199.00'
        ]
    );

    return $payment;

});

Route::post('/checkout',function(\Illuminate\Http\Request $request){


    //$result->success;


    //$result->customer->id;


    $amount = $request->amount;
    $nonce = $request->payment_method_nonce;


    $gateway = new Braintree\Gateway([
        'environment' => config('services.braintree.environment'),
        'merchantId' => config('services.braintree.merchantId'),
        'publicKey' => config('services.braintree.publicKey'),
        'privateKey' => config('services.braintree.privateKey')
    ]);



   // return $customer;

    $result = $gateway->customer()->create([
        'firstName' =>$request->firstName,
        'lastName' => $request->lastName,
        'company' => $request->company,
        'email' => $request->email,
        'phone' => $request->phone,
        'fax' =>$request->fax,
        'website' => $request->website,
        'paymentMethodNonce' => $nonce
    ]);
    if ($result->success) {
        echo($result->customer->id);
        echo($result->customer->paymentMethods[0]->token);
    } else {
        foreach($result->errors->deepAll() AS $error) {
            echo($error->code . ": " . $error->message . "\n");
        }
    }

    return back()->with('success_message','Transaction Successful. The ID is'.$result->customer->id);


//    $result = $gateway->transaction()->sale([
//        'amount' => $amount,
//        'paymentMethodNonce' => $nonce,
//        'options' => [
//            'submitForSettlement' => true
//        ]
//    ]);

//    if ($result->success) {
//        $transaction = $result->transaction;
//       // header("Location: transaction.php?id=" . $transaction->id);
//
//        return back()->with('success_message','Transaction Successful. The ID is'.$transaction->id);
//    } else {
//        $errorString = "";
//
//        foreach($result->errors->deepAll() as $error) {
//            $errorString .= 'Error: ' . $error->code . ": " . $error->message . "\n";
//        }
//
//       return back()->withErrors('An Error Occurred with the message:'.$request->messaage);
//    }


});
